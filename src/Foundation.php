<?php

namespace Leorain\Foundation;

use GuzzleHttp\Client;

class Foundation
{
    const API = "https://fundgz.1234567.com.cn/js/";

    public static function getFundDetail($code = 161725): object
    {
        $api          = self::API . $code . '.js';
        $client       = new Client();
        $resultString = $client->get($api)->getBody()->getContents();
        $resultString = str_replace("jsonpgz(", '', $resultString);
        $resultString = str_replace(");", '', $resultString);
        return json_decode($resultString);
    }
}